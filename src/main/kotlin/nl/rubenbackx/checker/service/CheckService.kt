package nl.rubenbackx.checker.service

import nl.rubenbackx.checker.data.Feedback
import nl.rubenbackx.checker.data.Visibility
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import org.springframework.web.multipart.MultipartFile

@Service
class CheckService {

    @Async
    fun checkFile(file: MultipartFile, key: String, submissionId: Long, scriptId: Long, score: Int, success: Boolean) {
        Thread.sleep((Math.random() * 10000).toLong())

        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON

        val request = HttpEntity(Feedback(
            submissionId = submissionId,
            scriptId = scriptId,
            textualFeedback = "Feedback :)",
            score = score,
            key = key,
            visibleFor = Visibility.STUDENT,
            success = success,
        ), headers)

        val template = RestTemplate()
        val response = template.postForEntity("http://localhost:8083/script/feedback", request, Void::class.java)
        println(request)
        println(response.statusCode)
        println(response.body)
        println(response.headers)
        println(response)
    }

}