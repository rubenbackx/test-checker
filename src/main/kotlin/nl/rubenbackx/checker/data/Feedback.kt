package nl.rubenbackx.checker.data

enum class Visibility {
    TEACHER, HEAD_TA, TA, STUDENT;

    override fun toString(): String = name
}

data class Feedback(
    val submissionId: Long,
    val scriptId: Long,
    val visibleFor: Visibility = Visibility.STUDENT,
    val textualFeedback: String,
    val score: Int,
    val key: String,
    val success: Boolean,
)