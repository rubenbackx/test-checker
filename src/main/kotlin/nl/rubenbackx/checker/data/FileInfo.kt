package nl.rubenbackx.checker.data

import org.springframework.web.multipart.MultipartFile

data class FileInfo(
    val file: MultipartFile,
    val submissionId: Long,
    val scriptId: Long,
    val key: String
)
