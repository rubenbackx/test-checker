package nl.rubenbackx.checker.controller

import nl.rubenbackx.checker.data.FileInfo
import nl.rubenbackx.checker.service.CheckService
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartHttpServletRequest

@Controller
@RequestMapping("/api")
class MainController(
    val checkService: CheckService
) {

    @RequestMapping(value = ["/check"], method = [RequestMethod.POST],
        consumes = [MediaType.MULTIPART_FORM_DATA_VALUE])
    fun checkFile(@ModelAttribute info: FileInfo, @RequestParam(required = false) score: Int = 0,
                    @RequestParam(required = false) success: Boolean?): ResponseEntity<Void> {
        println(info)
        println(info.file.originalFilename)
        checkService.checkFile(info.file, info.key, info.submissionId, info.scriptId, score, success ?: true)
        return ResponseEntity.ok(null)
    }

}