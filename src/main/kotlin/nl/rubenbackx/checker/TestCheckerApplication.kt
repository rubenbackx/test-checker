package nl.rubenbackx.checker

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TestCheckerApplication

fun main(args: Array<String>) {
    runApplication<TestCheckerApplication>(*args)
}
